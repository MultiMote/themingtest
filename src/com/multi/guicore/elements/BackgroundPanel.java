package com.multi.guicore.elements;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class BackgroundPanel extends JPanel implements MouseMotionListener, MouseListener {
    private Image img;
    private Point click_screen;
    private Point click_relative;

    private JFrame parent;

    public BackgroundPanel(Image img, JFrame parent) {
        this.parent = parent;
        this.img = img;
        this.setOpaque(false);
        this.addMouseMotionListener(this);
        this.addMouseListener(this);
    }

    @Override
    public void mouseDragged(MouseEvent event)
    {
        Point click = event.getLocationOnScreen();
        this.parent.setLocation(
                this.click_screen.x + (click.x - this.click_screen.x) - this.click_relative.x,
                this.click_screen.y + (click.y - this.click_screen.y) - this.click_relative.y
        );
    }


    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        this.click_screen = e.getLocationOnScreen();
        this.click_relative = e.getPoint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.click_screen = null;
        this.click_relative = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void paintComponent(Graphics g) {
        if(this.img!=null){
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.drawImage(this.img, 0, 0, this.getWidth(), this.getHeight(), null);
            g2.dispose();
        }
        super.paintComponent(g);
    }
}
