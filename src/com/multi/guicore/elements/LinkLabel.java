package com.multi.guicore.elements;

import com.multi.guicore.GuiCore;
import com.multi.guicore.data.PagesEnum;
import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.util.Map;

/**
 * Created by MultiMote on 03.11.2014.
 */
public class LinkLabel extends CustomLabel implements MouseListener {

    private final String link;
    private Font defFont;

    public LinkLabel(ThemedElement element){
        super(element);
        this.link = element.getPropertyString(PropertiesEnum.LINK);
        this.setFont(ElementCustomizer.generateFont(element));
        this.setSize(this.getPreferredSize());
        this.page = PagesEnum.parsePage(element.getProperty(PropertiesEnum.PAGE));
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
        this.addMouseListener(this);

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!(GuiCore.hooksRegistered() && GuiCore.getHooks().linkClick(this.link))) {
            GuiCore.openUrl(this.link);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {
        this.defFont = this.getFont();
        Map attribs = defFont.getAttributes();
        attribs.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        this.setFont(defFont.deriveFont(attribs));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.setFont(this.defFont);
    }
}
