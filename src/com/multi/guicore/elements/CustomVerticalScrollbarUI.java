package com.multi.guicore.elements;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;

/**
* Created by MultiMote on 08.11.2014.
*/
public class CustomVerticalScrollbarUI extends BasicScrollBarUI {
    private Image thumbBg, upButton, downButton;
    public CustomVerticalScrollbarUI(Image thumbBg, Image upButton, Image downButton){
        this.thumbBg = thumbBg;
        this.upButton = upButton;
        this.downButton = downButton;
    }

    @Override
    protected void paintTrack( Graphics g, JComponent c, Rectangle trackBounds ){}

    @Override
    protected void paintThumb( Graphics g, JComponent c, Rectangle thumbBounds ) {
        int w = thumbBounds.width;
        int h = thumbBounds.height;

        if (c.isEnabled() && this.thumbBg != null) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.translate(thumbBounds.x, thumbBounds.y);

            g2.drawImage(this.thumbBg, 0, 0, 17, 4, 0, 0, 17, 4, null);

            g2.drawImage(this.thumbBg, 0, 4, 17, h/2-3, 0, 5, 17, 9, null);

            g2.drawImage(this.thumbBg, 0, h/2-3, 17, h/2+4, 0, 10, 17, 17, null);

            g2.drawImage(this.thumbBg, 0, h/2+4, 17, h-4, 0, 18, 17, 22, null);

            g2.drawImage(this.thumbBg, 0, h-4, 17, h, 0, 23, 17, 27, null);

            g2.dispose();
        }else super.paintThumb(g, c, thumbBounds);


    }

    protected JButton createDecreaseButton( int orientation )
    {
        return new ScrollbarCustomArrowButton(orientation, this.upButton);
    }

    protected JButton createIncreaseButton( int orientation )
    {
        return new ScrollbarCustomArrowButton(orientation, this.downButton);

    }
}
