package com.multi.guicore.elements;

import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class TexturedButton extends JButton {


    Image tex;
    boolean staticImage;
    protected ThemedElement themed;

    public TexturedButton(ThemedElement element){
        this.themed = element;
        this.setBounds(element.getBounds());

        ElementCustomizer.setFont(this, element);
        if(element.getPropertyString(PropertiesEnum.TEXT) != null){
            this.setText(element.getPropertyString(PropertiesEnum.TEXT));
        }

        if(this.themed.getPropertyImage(PropertiesEnum.BACKGROUND_STATIC)!=null){
            this.staticImage = true;
            this.tex = this.themed.getPropertyImage(PropertiesEnum.BACKGROUND_STATIC);
        }else this.tex = this.themed.getPropertyImage(PropertiesEnum.BACKGROUND);

        this.setBorderPainted(false);
        this.setContentAreaFilled(false);
        this.setFocusable(false);
    }

    @Override
    public void paintComponent(Graphics g) {
        if(!this.staticImage){
            if(this.getModel().isPressed())this.tex =  this.themed.getPropertyImage(PropertiesEnum.BACKGROUND_PRESSED);
            else if(this.getModel().isRollover()) this.tex =  this.themed.getPropertyImage(PropertiesEnum.BACKGROUND_HOVER);
            else this.tex =  this.themed.getPropertyImage(PropertiesEnum.BACKGROUND);
        }

        if(this.tex!=null){
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.drawImage(this.tex, 0, 0, this.getWidth(), this.getHeight(), null);
            g2.dispose();
        }
        super.paintComponent(g);
    }

}
