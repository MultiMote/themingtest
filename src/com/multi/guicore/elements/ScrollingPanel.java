package com.multi.guicore.elements;

import javax.swing.*;

/**
 * Created by MultiMote on 09.11.2014.
 */
public class ScrollingPanel extends JScrollPane{
    public ScrollingPanel(JPanel panel){

        panel.setOpaque(false);

        this.setOpaque(false);
        this.setBorder(null);

        this.getVerticalScrollBar().setOpaque(false);
        this.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
        this.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);

        this.setViewportView(panel);
        this.getViewport().setOpaque(false);
    }
}
