package com.multi.guicore.elements;

import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by MultiMote on 02.11.2014.
 */
public class TexturedTextBox extends JTextField{

    Image bg;

    public TexturedTextBox(ThemedElement element){
        this.setBounds(element.getBounds());
        this.setCaretColor(new Color(element.getPropertyInt(PropertiesEnum.CARET_COLOR)));

        ElementCustomizer.setFont(this, element);
        this.bg = element.getPropertyImage(PropertiesEnum.BACKGROUND);
        this.setBackground(new Color(0F, 0F, 0F, 0F)); //Magic. Do not touch.
        EmptyBorder inset = new EmptyBorder(element.getPropertyInt(PropertiesEnum.INSET_TOP), element.getPropertyInt(PropertiesEnum.INSET_LEFT), element.getPropertyInt(PropertiesEnum.INSET_BOTTOM), element.getPropertyInt(PropertiesEnum.INSET_RIGHT));
        this.setBorder(inset);
    }

    @Override
    protected void paintComponent(Graphics g) {
        if(this.bg!=null){
            this.getParent().repaint(); //костыль
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.drawImage(this.bg, 0, 0, this.getWidth(), this.getHeight(), null);
            g2.dispose();
        }
        super.paintComponent(g);
    }
}
