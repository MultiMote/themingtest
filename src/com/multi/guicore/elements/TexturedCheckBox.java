package com.multi.guicore.elements;

import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by MultiMote on 04.11.2014.
 */
public class TexturedCheckBox extends JCheckBox {

    private Icon[] pics;

    public TexturedCheckBox(ThemedElement element){
        this.setLocation(element.xPos, element.yPos);
        this.pics = new Icon[]{
                element.getPropertyImage(PropertiesEnum.BACKGROUND_CHECKBOX_NORMAL) == null ? null : new ImageIcon(element.getPropertyImage(PropertiesEnum.BACKGROUND_CHECKBOX_NORMAL)),
                element.getPropertyImage(PropertiesEnum.BACKGROUND_CHECKBOX_SELECTED) == null ? null : new ImageIcon(element.getPropertyImage(PropertiesEnum.BACKGROUND_CHECKBOX_SELECTED))
        };
        String text = element.getPropertyString(PropertiesEnum.TEXT) != null ? element.getPropertyString(PropertiesEnum.TEXT) : "FIX ME";
        this.setText(text);
        ElementCustomizer.setFont(this, element);

        this.setIcon(this.pics[0]);

        this.setSize(this.getPreferredSize());
        this.setContentAreaFilled(false);
        this.setFocusable(false);

    }

    @Override
    protected void paintComponent(Graphics g) {
        if(this.getModel().isSelected())
            this.setIcon(this.pics[1]);
        else
            this.setIcon(this.pics[0]);

        super.paintComponent(g);
    }
}
