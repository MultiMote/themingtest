package com.multi.guicore.elements;

import com.multi.guicore.data.PagesEnum;
import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;

/**
 * Created by MultiMote on 03.11.2014.
 */
public class CustomLabel extends JLabel {

    public PagesEnum page;

    public CustomLabel(ThemedElement element) {
        this.setLocation(element.xPos, element.yPos);
        String text = element.getPropertyString(PropertiesEnum.TEXT) != null ? element.getPropertyString(PropertiesEnum.TEXT) : "FIX ME";
        this.setText(text);
        ElementCustomizer.setFont(this, element);
        this.setSize(this.getPreferredSize());
        this.page = PagesEnum.parsePage(element.getProperty(PropertiesEnum.PAGE));
    }
}
