package com.multi.guicore.elements;

import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ServerAddress;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Created by MultiMote on 05.11.2014.
 */
public class ServerList extends JScrollPane{
    private List<ServerAddress> addressList;
    private MainFrame parent;
    protected ThemedElement themed;
    private JPanel buttonPanel;

    public ServerList(MainFrame parent, ThemedElement element, List<ServerAddress> addressList){
        this.parent = parent;
        this.themed = element;
        this.setBounds(element.getBounds());
        this.addressList = addressList;
        this.setOpaque(false);
        this.setBorder(null);
        this.getVerticalScrollBar().setUI(new CustomVerticalScrollbarUI(
                themed.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_LIST_THUMB),
                themed.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_LIST_UP_BUTTON),
                themed.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_LIST_DOWN_BUTTON)
        ));
        this.getVerticalScrollBar().setOpaque(false);
        this.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
        this.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);


        this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout(null);
        this.buttonPanel.setOpaque(false);
        this.setViewportView(this.buttonPanel);
        this.getViewport().setOpaque(false);
    }


    @Override
    protected void paintComponent(Graphics g) {
        if(themed.getPropertyImage(PropertiesEnum.BACKGROUND)!=null){
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.drawImage(themed.getPropertyImage(PropertiesEnum.BACKGROUND), 0, 0, this.getWidth(), this.getHeight(), null);
            g2.dispose();
        }
        super.paintComponent(g);
    }

    @Override
    public void repaint() {
        if(this.getParent() == null || this.addressList == null){ super.repaint(); return; }

        this.buttonPanel.removeAll();

        int servers = this.addressList.size();
        int nodeWidth = this.themed.getPropertyInt(PropertiesEnum.WIDTH_SERVER_LIST_NODE);
        int nodeHeight = this.themed.getPropertyInt(PropertiesEnum.HEIGHT_SERVER_LIST_NODE);
        int shiftLeft = this.themed.getPropertyInt(PropertiesEnum.MARGIN_LEFT_SERVER_LIST_NODE);
        int shiftTop = this.themed.getPropertyInt(PropertiesEnum.MARGIN_TOP_SERVER_LIST_NODE);
        int spacing = this.themed.getPropertyInt(PropertiesEnum.SPACING_NODE);

        for(int i = 0; i < servers; i++) {
            ServerNode button = new ServerNode(this.parent, this.themed, shiftLeft, shiftTop + (nodeHeight + spacing) * i, nodeWidth, nodeHeight, this.addressList.get(i));
            this.buttonPanel.add(button);
        }

        this.buttonPanel.setSize(nodeWidth, (nodeHeight + spacing) * servers - spacing + shiftTop*2);
        this.buttonPanel.setPreferredSize(new Dimension(nodeWidth, (nodeHeight + spacing) * servers - spacing + shiftTop*2));


       //  for(Component com : buttonPanel.getComponents()) com.repaint();

        super.repaint();
    }

}
