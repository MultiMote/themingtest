package com.multi.guicore.elements;

import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;

/**
* Created by MultiMote on 08.11.2014.
*/
public class ScrollbarCustomArrowButton extends BasicArrowButton {

    private final Image img;

    public ScrollbarCustomArrowButton(int direction, Image img) {
        super(direction);
        this.img = img;

    }

    @Override
    public void paint(Graphics g) {

        if(this.img!=null){
            this.getParent().repaint();
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(
                    this.isEnabled() ?
                    new Color(1F, 1F, 1F, 1F) :
                    new Color(0.5F, 0.5F, 0.5F, 1F)
            );
            g2.drawImage(this.img, 0, 0, this.getSize().width, this.getSize().height, null);
            g2.dispose();
        }else super.paint(g);
    }
}
