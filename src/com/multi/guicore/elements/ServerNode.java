package com.multi.guicore.elements;

import com.multi.guicore.data.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by MultiMote on 05.11.2014.
 */
public class ServerNode extends JPanel implements MouseListener, ActionListener{
    private int nodeWidth;
    private int nodeHeight;
    private int nodeX;
    private int nodeY;
    private ServerAddress address;
    private MainFrame parent;
    private Image bg;

    public ServerNode(MainFrame parent, ThemedElement element, int x, int y, int width, int height, ServerAddress address){
        this.parent = parent;
        this.nodeWidth = width;
        this.nodeHeight = height;
        this.nodeX = x;
        this.nodeY = y;
        this.address = address;
        this.bg = element.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_LIST_NODE);
        this.setLayout(null);
        this.setOpaque(false);

        //this.setBackground(new Color(0x787878));
        this.setLocation(x, y);
        this.setSize(width, height);
        this.setPreferredSize(new Dimension(width, height));

        JLabel name = new JLabel(address.getName());
        name.setLocation(10, 5);
        name.setSize(name.getPreferredSize());
        name.setForeground(element.getPropertyColor(PropertiesEnum.FONT_COLOR));

        JLabel ip = new JLabel(address.getIP());
        ip.setLocation(10, 30);
        ip.setSize(ip.getPreferredSize());
        ip.setForeground(element.getPropertyColor(PropertiesEnum.FONT_COLOR));


        JLabel ping = new JLabel(address.getServerInfo().translateState(ElementsEnum.SERVER_LIST, element));

        ping.setForeground(element.getPropertyColor(PropertiesEnum.FONT_COLOR));
        ping.setSize(ping.getPreferredSize());
        ping.setLocation(this.nodeWidth - ping.getWidth() - 2, this.nodeHeight - ping.getHeight());

        JLabel users = new JLabel(address.getServerInfo().getPlayers().size() + "/" + address.getServerInfo().getPlayersMax());
        users.setForeground(element.getPropertyColor(PropertiesEnum.FONT_COLOR));
        users.setSize(users.getPreferredSize());
        users.setLocation(this.nodeWidth - users.getWidth() - 2, this.nodeHeight - 12 - users.getHeight());


        ServerListInfoButton info = new ServerListInfoButton(element.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_LIST_INFO_BUTTON));
        info.setBounds(
                this.nodeWidth - element.getPropertyInt(PropertiesEnum.WIDTH_SERVER_LIST_INFO_BUTTON) - element.getPropertyInt(PropertiesEnum.X_SERVER_LIST_INFO_BUTTON),
                element.getPropertyInt(PropertiesEnum.Y_SERVER_LIST_INFO_BUTTON),
                element.getPropertyInt(PropertiesEnum.WIDTH_SERVER_LIST_INFO_BUTTON),
                element.getPropertyInt(PropertiesEnum.HEIGHT_SERVER_LIST_INFO_BUTTON));
        info.addActionListener(this);

        this.add(name);
        this.add(ip);
        this.add(users);
        this.add(ping);
        this.add(info);


        this.addMouseListener(this);
    }

/*    @Override
    public void repaint() {
        if(this.getParent() == null) return;
        users.setText(address.getServerInfo().getPlayers().size() + "/" + address.getServerInfo().getPlayersMax());
        users.setSize(users.getPreferredSize());
        users.setLocation(this.nodeWidth - users.getWidth(), this.nodeHeight - users.getHeight());

    }
*/
    @Override
    public void paintComponent(Graphics g) {
        if(this.bg!=null){
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.drawImage(this.bg, 0, 0, this.getWidth(), this.getHeight(), null);
            g2.dispose();
        }
        super.paintComponent(g);
    }

    public ServerAddress getServer() {
        return address;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       this.parent.serverInfoClick(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.parent.serverClick(this);
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    private class ServerListInfoButton extends JButton {
        Image tex;

        public ServerListInfoButton(Image bg){
            this.tex = bg;
            this.setBorderPainted(false);
            this.setContentAreaFilled(false);
            this.setFocusable(false);
        }

        @Override
        public void paintComponent(Graphics g) {
            if(this.tex!=null){
                Graphics2D g2 = (Graphics2D) g.create();
                g2.setColor(new Color(1F, 1F, 1F, 1F));
                g2.drawImage(this.tex, 0, 0, this.getWidth(), this.getHeight(), null);
                g2.dispose();
            }
            super.paintComponent(g);
        }

    }

}
