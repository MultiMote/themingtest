package com.multi.guicore.elements;

import com.multi.guicore.GuiCore;
import com.multi.guicore.data.*;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class MainFrame extends JFrame implements ActionListener {

    public static String ENDL = "\n\r";

    private BackgroundPanel main_panel;
    private TexturedButton close_button;
    private TexturedButton minimize_button;
    private TexturedButton login_button;
    private TexturedButton settings_button;
    private TexturedButton home_button;
    private TexturedButton info_button;

    private ServerButton server_list_button;


    private TexturedTextBox username_field;
    private TexturedPassBox password_field;

    private TexturedCheckBox password_save_checkbox;

    private ServerList server_list;

    private CustomLabel[] labels;
    private LinkLabel[] links;
    private LittlePic[] pics;

    private JLabel server_info_title;
    private JTextArea server_info_text;
    private ScrollingPanel server_info_scroller;

    private PagesEnum page;

    private ArrayList<ServerAddress> availableServers = new ArrayList<ServerAddress>();
    private ServerAddress currentServer;
    private ServerAddress currentServerInfo;

    public void init() {
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                GuiCore.shutdown();
            }
        });

        HashMap<ElementsEnum, ThemedElement> elements = WindowTheme.instance.elementMap;
        ThemedElement windowElement = elements.get(ElementsEnum.MAIN_WINDOW);

        this.setTitle(ElementCustomizer.chooseRandomString(windowElement, PropertiesEnum.TEXT));
        this.setUndecorated(true);
        //this.setBackground(new Color(0, 0, 0, 0)); //todo
        this.setResizable(false);
        this.setSize(windowElement.width, windowElement.height);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());
        if (windowElement.getPropertyImage(PropertiesEnum.ICON) != null) this.setIconImage(windowElement.getPropertyImage(PropertiesEnum.ICON));
        this.main_panel = new BackgroundPanel(windowElement.getPropertyImage(PropertiesEnum.BACKGROUND), this);
        this.main_panel.setLayout(null);

        this.close_button = new TexturedButton(elements.get(ElementsEnum.CLOSE_BUTTON));
        this.close_button.addActionListener(this);

        this.login_button = new TexturedButton(elements.get(ElementsEnum.LOGIN_BUTTON));
        this.login_button.addActionListener(this);

        this.server_list_button = new ServerButton(elements.get(ElementsEnum.SERVER_LIST_BUTTON), this.currentServer);
        this.server_list_button.addActionListener(this);

        this.server_list = new ServerList(this, elements.get(ElementsEnum.SERVER_LIST), this.availableServers);

        ThemedElement serverInfo = elements.get(ElementsEnum.SERVER_INFO_PAGE);

        this.server_info_title = new JLabel();
        ElementCustomizer.customizeLabel(
                this.server_info_title,
                serverInfo,
                PropertiesEnum.X_SERVER_INFO_TITLE,
                PropertiesEnum.Y_SERVER_INFO_TITLE,
                PropertiesEnum.COLOR_SERVER_INFO_TITLE,
                PropertiesEnum.FONT_SIZE_SERVER_INFO_TITLE,
                PropertiesEnum.FONT_FAMILY_SERVER_INFO_TITLE
                );

        JPanel server_info_panel = new JPanel();
        server_info_panel.setLayout(new GridLayout(1, 1));
        this.server_info_text = new JTextArea();

        ElementCustomizer.setFont(this.server_info_text, serverInfo,
                PropertiesEnum.COLOR_SERVER_INFO_DESCRIPTION,
                PropertiesEnum.FONT_SIZE_SERVER_INFO_DESCRIPTION,
                PropertiesEnum.FONT_FAMILY_SERVER_INFO_DESCRIPTION
        );

        this.server_info_text.setLineWrap(true);
        this.server_info_text.setOpaque(false);
        this.server_info_text.setEditable(false);
        ((DefaultCaret) this.server_info_text.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        server_info_panel.add(this.server_info_text);

        this.server_info_scroller = new ScrollingPanel(server_info_panel);
        this.server_info_scroller.setBounds(
                serverInfo.getPropertyInt(PropertiesEnum.X_SERVER_INFO_DESCRIPTION),
                serverInfo.getPropertyInt(PropertiesEnum.Y_SERVER_INFO_DESCRIPTION),
                serverInfo.getPropertyInt(PropertiesEnum.WIDTH_SERVER_INFO_DESCRIPTION),
                serverInfo.getPropertyInt(PropertiesEnum.HEIGHT_SERVER_INFO_DESCRIPTION)
        );
        this.server_info_scroller.getVerticalScrollBar().setUI(new CustomVerticalScrollbarUI(
                serverInfo.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_INFO_THUMB),
                serverInfo.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_INFO_UP_BUTTON),
                serverInfo.getPropertyImage(PropertiesEnum.BACKGROUND_SERVER_INFO_DOWN_BUTTON)
        ));
        this.server_info_scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        if (elements.containsKey(ElementsEnum.MINIMIZE_BUTTON)) {
            this.minimize_button = new TexturedButton(elements.get(ElementsEnum.MINIMIZE_BUTTON));
            this.minimize_button.addActionListener(this);
        }

        if (elements.containsKey(ElementsEnum.SETTINGS_BUTTON)) {
            this.settings_button = new TexturedButton(elements.get(ElementsEnum.SETTINGS_BUTTON));
            this.settings_button.addActionListener(this);
        }

        if (elements.containsKey(ElementsEnum.HOME_BUTTON)) {
            this.home_button = new TexturedButton(elements.get(ElementsEnum.HOME_BUTTON));
            this.home_button.addActionListener(this);
        }

        if (elements.containsKey(ElementsEnum.INFO_BUTTON)) {
            this.info_button = new TexturedButton(elements.get(ElementsEnum.INFO_BUTTON));
            this.info_button.addActionListener(this);
        }

        this.username_field = new TexturedTextBox(elements.get(ElementsEnum.USERNAME_FIELD));
        this.username_field.setDocument(new TextFilter("[^a-zA-Z0-9_-]"));

        this.password_field = new TexturedPassBox(elements.get(ElementsEnum.PASSWORD_FIELD));

        this.password_save_checkbox = new TexturedCheckBox(elements.get(ElementsEnum.PASSWORD_SAVE_CHECKBOX));
        this.password_save_checkbox.addActionListener(this);

        WindowTheme.instance.checkMaps();

        if (!WindowTheme.instance.labelMap.isEmpty()) {
            this.labels = new CustomLabel[WindowTheme.instance.labelMap.size()];
            for (int i = 0; i < this.labels.length; i++) {
                if (WindowTheme.instance.labelMap.containsKey(i))
                    this.labels[i] = new CustomLabel(WindowTheme.instance.labelMap.get(i));
            }
        }

        if (!WindowTheme.instance.linkMap.isEmpty()) {
            this.links = new LinkLabel[WindowTheme.instance.linkMap.size()];
            for (int i = 0; i < this.links.length; i++) {
                if (WindowTheme.instance.linkMap.containsKey(i))
                    this.links[i] = new LinkLabel(WindowTheme.instance.linkMap.get(i));
            }
        }

        if (!WindowTheme.instance.picMap.isEmpty()) {
            this.pics = new LittlePic[WindowTheme.instance.picMap.size()];
            for (int i = 0; i < this.pics.length; i++) {
                if (WindowTheme.instance.picMap.containsKey(i))
                    this.pics[i] = new LittlePic(WindowTheme.instance.picMap.get(i));
            }
        }

        this.add(this.main_panel);
        this.setPage(PagesEnum.MAIN);
    }

    /////////////////////////////////////////////////////
    /// Data workers
    /////////////////////////////////////////////////////

    public void setUsername(String user) {
        this.username_field.setText(user);
    }

    public void setPassword(String pass) {
        this.password_field.setText(pass);
    }

    public void setCurrentServer(ServerAddress currentServer) {
        this.currentServer = currentServer;
        this.server_list_button.setAddress(currentServer);

    }

    public ServerAddress getCurrentServer() {
        return currentServer;
    }

    public void setCurrentServerInfo(ServerAddress currentServerInfo) {
        this.currentServerInfo = currentServerInfo;
    }

    public ServerAddress getCurrentServerInfo() {
        return currentServerInfo;
    }

    public boolean isPassSaving() {
        return this.password_save_checkbox.isSelected();
    }

    public void setPassSaving(boolean is) {
        this.password_save_checkbox.setSelected(is);
    }

    public void addServerList(ArrayList<ServerAddress> servers) {
        this.availableServers.addAll(servers);
    }

    public ArrayList<ServerAddress> getServers() {
        return availableServers;
    }

    public void clearServerList() {
        this.availableServers.clear();
    }

    public void updateServerList(){
        this.server_list.repaint();
    }

    public void updateServerButton(){
        this.server_list_button.updateLabels();
    }

    public void updateServerInfoDescription(){
        if(this.currentServerInfo == null) return;
        this.server_info_title.setText(this.currentServerInfo.getName());
        String str = this.currentServerInfo.getServerInfo().getDescription();

        str += ENDL;
        str += ENDL + this.getCurrentServerInfo().getServerInfo().translateState(ElementsEnum.SERVER_INFO_PAGE, WindowTheme.instance.elementMap.get(ElementsEnum.SERVER_INFO_PAGE)) + ENDL;
        str += ENDL;
        java.util.List<String> players = this.currentServerInfo.getServerInfo().getPlayers();
        str += "Игроки" + " (" + players.size() + "/" + this.currentServerInfo.getServerInfo().getPlayersMax() + ")" + ":" + ENDL;
        for(String s : players)
            str += s + ENDL;

        this.server_info_text.setText(str);
    }

    public void updateAllDisplays(){
        this.updateServerList();
        this.updateServerButton();
        this.updateServerInfoDescription();
    }

    /////////////////////////////////////////////////////

    public PagesEnum getPage() {
        return page;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.close_button) {
            if (!(GuiCore.hooksRegistered() && GuiCore.getHooks().closeClick())) {
                GuiCore.shutdown();
            }

        } else if (e.getSource() == this.minimize_button) {
            if (!(GuiCore.hooksRegistered() && GuiCore.getHooks().minimizeClick())) {
                setState(Frame.ICONIFIED);
            }
        } else if (e.getSource() == this.login_button) {
            if (GuiCore.hooksRegistered()) {
                GuiCore.getHooks().loginButtonClick(
                        this.username_field.getText(),
                        String.valueOf(this.password_field.getPassword()),
                        this.currentServer);
            }

        } else if (e.getSource() == this.server_list_button) {
            if (!(GuiCore.hooksRegistered() && GuiCore.getHooks().serverListButtonClick())) {
                this.setPage(PagesEnum.SERVERS);
            }
        } else if (e.getSource() == this.settings_button) {
            this.setPage(PagesEnum.SETTINGS);
        } else if (e.getSource() == this.home_button) {
            this.setPage(PagesEnum.MAIN);
        } else if (e.getSource() == this.info_button) {
            this.setPage(PagesEnum.INFO);
        } else if (e.getSource() == this.password_save_checkbox) {
            if (GuiCore.hooksRegistered()) {
                GuiCore.getHooks().passwordCheckboxClick(this.password_save_checkbox.isSelected());
            }
        }
    }

    public void serverClick(ServerNode serverNode) {
        if (!(GuiCore.hooksRegistered() && GuiCore.getHooks().serverClick(serverNode.getServer()))) {
            this.setCurrentServer(serverNode.getServer());
            this.setPage(PagesEnum.MAIN);
        }
    }

    public void serverInfoClick(ServerNode serverNode) {
        if (!(GuiCore.hooksRegistered() && GuiCore.getHooks().serverInfoClick(serverNode.getServer()))) {
            this.setCurrentServerInfo(serverNode.getServer());
            this.setPage(PagesEnum.SERVER_INFO);
        }
    }

    public void setPage(PagesEnum page) {
        if (GuiCore.hooksRegistered() && GuiCore.getHooks().changePage(this.getPage(), page)) return;
        this.page = page;
        this.main_panel.removeAll();
        this.main_panel.add(this.close_button);
        if (this.minimize_button != null) this.main_panel.add(this.minimize_button);

        if (page == PagesEnum.MAIN) {
            this.main_panel.add(this.username_field);
            this.main_panel.add(this.password_field);
            this.main_panel.add(this.login_button);
            this.main_panel.add(this.server_list_button);
            this.main_panel.add(this.password_save_checkbox);
        } else if (page == PagesEnum.SETTINGS) {

        } else if (page == PagesEnum.SERVERS) {
            this.main_panel.add(this.server_list);
            this.server_list.repaint();
        } else if (page == PagesEnum.SERVER_INFO) {
            if(this.currentServerInfo != null){
                this.updateServerInfoDescription();

            }else this.server_info_title.setText("Wrong server!");
            ElementCustomizer.normalizeSize(this.server_info_title);
            this.main_panel.add(this.server_info_scroller);
            this.main_panel.add(this.server_info_title);


        }


        if (page != PagesEnum.SETTINGS) {
            if (this.settings_button != null) this.main_panel.add(this.settings_button);
        }

        if (page != PagesEnum.MAIN) {
            if (this.home_button != null) this.main_panel.add(this.home_button);
        }

        if (page != PagesEnum.INFO) {
            if (this.info_button != null) this.main_panel.add(this.info_button);
        }

        if (this.labels != null) {
            for (CustomLabel label : this.labels) {
                if (label != null && label.page == page) {
                    this.main_panel.add(label);
                }
            }
        }

        if (this.links != null) {
            for (CustomLabel link : this.links) {
                if (link != null && (link.page == PagesEnum.ALL ||link.page == page)) {
                    this.main_panel.add(link);
                }
            }
        }

        if (this.pics != null) {

            for (LittlePic pic : this.pics) {
                if (pic != null && (pic.page == PagesEnum.ALL || pic.page == page)) {
                    this.main_panel.add(pic);
                }
            }
        }

        this.main_panel.revalidate();
        this.main_panel.repaint();
    }


}
