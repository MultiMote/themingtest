package com.multi.guicore.elements;

import com.multi.guicore.data.PagesEnum;
import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by MultiMote on 03.11.2014.
 */
public class LittlePic extends JLabel {

    public PagesEnum page;
    private final Image bg;

    public LittlePic(ThemedElement element) {
        this.setBounds(element.getBounds());
        this.bg = element.getPropertyImage(PropertiesEnum.BACKGROUND);
        this.page = PagesEnum.parsePage(element.getProperty(PropertiesEnum.PAGE));

    }

    @Override
    protected void paintComponent(Graphics g) {
        if(this.bg!=null){
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(new Color(1F, 1F, 1F, 1F));
            g2.drawImage(this.bg, 0, 0, this.getWidth(), this.getHeight(), null);
            g2.dispose();
        }
    }
}
