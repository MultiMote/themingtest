package com.multi.guicore.elements;

import com.multi.guicore.data.ElementsEnum;
import com.multi.guicore.data.ServerAddress;
import com.multi.guicore.data.ServerState;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;

/**
 * Created by MultiMote on 06.11.2014.
 */
public class ServerButton extends TexturedButton {
    private ServerAddress address;
    public ServerButton(ThemedElement element, ServerAddress address){
        super(element);
        this.address = address;
        this.setLayout(null);
        this.updateLabels();

    }

    public void setAddress(ServerAddress address) {
        this.firePropertyChange("server", this.address, address);
        this.address = address;
        this.updateLabels();
    }

    public void updateLabels(){
        if(this.address == null) return;
        this.removeAll();

        JLabel serverName = new JLabel(address.getName());
        ElementCustomizer.setFont(serverName, themed);

        serverName.setLocation(5, 0);
        serverName.setSize(serverName.getPreferredSize());

        JLabel serverPing = new JLabel( address.getServerInfo().translateState(ElementsEnum.SERVER_LIST_BUTTON ,themed) );
        ElementCustomizer.setFont(serverPing, themed);

        serverPing.setSize(serverPing.getPreferredSize());
        serverPing.setLocation(5, this.getHeight() - serverPing.getHeight());

        this.add(serverName);
        this.add(serverPing);

        this.revalidate();
        this.repaint();
    }
}
