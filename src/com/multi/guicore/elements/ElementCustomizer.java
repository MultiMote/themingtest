package com.multi.guicore.elements;

import com.multi.guicore.data.PropertiesEnum;
import com.multi.guicore.data.ThemedElement;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by MultiMote on 06.11.2014.
 */
public class ElementCustomizer {

    public static Random RNG = new Random();


    public static void setFont(JComponent what, ThemedElement element,  PropertiesEnum color_property,  PropertiesEnum size_property,  PropertiesEnum family_property){
        what.setForeground(new Color(element.getPropertyInt(color_property)));
        what.setFont(generateFont(element, size_property, family_property));
    }

    public static void setFont(JComponent what, ThemedElement element){
        setFont(what, element, PropertiesEnum.FONT_COLOR, PropertiesEnum.FONT_SIZE, PropertiesEnum.FONT_FAMILY);
    }

    public static Font generateFont(ThemedElement element){
       return generateFont(element, PropertiesEnum.FONT_SIZE, PropertiesEnum.FONT_FAMILY);
    }

    public static Font generateFont(ThemedElement element, PropertiesEnum size_property,  PropertiesEnum family_property){
        int fontSize = element.getPropertyInt(size_property);
        String font = element.getProperty(family_property) != null ? element.getProperty(family_property).toString() : null;
        return new Font(font != null ? font : "Verdana", Font.PLAIN, fontSize > 0 ? fontSize : 12);
    }

    public static void customizeLabel(JLabel label, ThemedElement element, PropertiesEnum x_property,  PropertiesEnum y_property,  PropertiesEnum color_property,  PropertiesEnum size_property,  PropertiesEnum family_property){
        label.setLocation(element.getPropertyInt(x_property), element.getPropertyInt(y_property));
        label.setForeground(new Color(element.getPropertyInt(color_property)));
        int fontSize = element.getPropertyInt(size_property);
        String font = element.getProperty(family_property) != null ? element.getProperty(family_property).toString() : null;
        label.setFont(new Font(font != null ? font : "Verdana", Font.PLAIN, fontSize > 0 ? fontSize : 12));
        label.setSize(label.getPreferredSize());
    }

    public static void normalizeSize(JComponent component){
        component.setSize(component.getPreferredSize());
    }

    public static String chooseRandomString(ThemedElement element, PropertiesEnum text_property){
        String text = element.getPropertyString(text_property);
        if(text == null) return null;
        if(!text.contains(";")) return text;
        String[] strings = text.split(";");
        return strings[RNG.nextInt(strings.length)];
    }
}
