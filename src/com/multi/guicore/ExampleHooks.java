package com.multi.guicore;

import com.multi.guicore.data.IGuiHooks;
import com.multi.guicore.data.PagesEnum;
import com.multi.guicore.data.ServerAddress;

/**
 * Created by MultiMote on 03.11.2014.
 */
public class ExampleHooks implements IGuiHooks {

    /**
     * Return true to cancel event.
     */

    @Override
    public void terminateLauncher() {

    }

    @Override
    public void guiDisplayed() {

    }

    @Override
    public boolean closeClick() {
        GuiCore.info(this.getClass(), "Close fired!");
        return false;
    }

    @Override
    public boolean minimizeClick() {
        return false;
    }

    @Override
    public void passwordCheckboxClick(boolean selected) {

    }

    @Override
    public boolean linkClick(String link) {
        return false;
    }

    @Override
    public boolean changePage(PagesEnum from, PagesEnum to) {
        return false;
    }

    @Override
    public void loginButtonClick(String text, String password, ServerAddress server) {
        GuiCore.severe(this.getClass(), text + " " + password + " " + server);
    }

    @Override
    public boolean serverClick(ServerAddress address) {
        return false;
    }

    @Override
    public boolean serverInfoClick(ServerAddress address) {
        GuiCore.severe(this.getClass(), "Info event for " + address);
        return false;
    }

    @Override
    public boolean serverListButtonClick() {
        return false;
    }
}
