#��������� ��������� ��� �����������:
---

main_window:

    background - ��� ���� - TEX
    icon - ������ ���� - TEX
    title - ��������� ���� - RSTR/STR
    width - ������ ���� - NUM
    height - ������ ���� - NUM
    
*_button:

    x - ��������� � ���� - NUM
    y - ��������� � ���� - NUM
    width - ������ ������ - NUM
    height - ������ ������ - NUM
    static - ���������� ��� ������, ���� ��������, �� ����������� ���� �� ������������ - TEX
    background - ��� ������ - TEX
    hover - ��� ��� ��������� ����� - TEX
    pressed - ��� ��� ������� ������ - TEX
    text - ����� ������ - STR
    textSize - ������ ������ ������ - NUM
    textColor - ���� ������ ������ - COLOR
    font - ��������� ������� ������� ������ - STR
    
*_field:

    x - ��������� � ���� - NUM
    y - ��������� � ���� - NUM
    width - ������ ���� - NUM
    height - ������ ���� - NUM
    textMarginLeft - ������ ������ �� ������ ���� - NUM
    textMarginRight - ������ ������ �� ������� ���� - NUM
    textMarginTop - ������ ������ �� �������� ���� - NUM
    textMarginBottom - ������ ������ �� ������� ���� - NUM
    textColor - ���� ������ ���� - COLOR
    textSize - ������ ������ ���� - NUM
    font - ��������� ������� ������ ���� - STR
    pointerColor - ���� ������� | - COLOR
    background - ��� ���� - TEX
    
server_list:
    
    x - ��������� � ���� - NUM
    y - ��������� � ���� - NUM
    width - ������ ������ - NUM
    height - ������ ������ - NUM
    background - ��� ������ - TEX
    infoButtonX  - ��������� � ���� �������������� ������ - NUM
    infoButtonY  - ��������� � ���� �������������� ������ - NUM
    infoButtonWidth - ������ �������������� ������ - NUM
    infoButtonHeight - ������ �������������� ������ - NUM
    infoButtonBackground - ��� �������������� ������ - TEX
    nodeMarginLeft
    nodeMarginTop
    nodeSpacing
    nodeWidth
    nodeHeight
    nodeBackground
    nodeTextColor
    upButtonBackground
    downButtonBackground
    thumbBackground
    
label_*:

    x - ��������� � ���� - NUM
    y - ��������� � ���� - NUM
    text - ����� ������� - STR
    textColor - ���� ������ ������� - COLOR
    textSize - ������ ������ ������� - NUM
    font - ��������� ������� ������� - STR
    page - ��������, �� ������� ����� ������������ ������� - PAGE
    
link_*:

    x - ��������� � ���� - NUM
    y - ��������� � ���� - NUM
    text - ����� ������ - STR
    link - ����� ������ - STR
    textColor - ���� ������ ������ - COLOR
    textSize - ������ ������ ������ - NUM
    font - ��������� ������� ������ - STR
    page - ��������, �� ������� ����� ������������ ������ - PAGE
    
image_*:

    x - ��������� � ���� - NUM
    y - ��������� � ���� - NUM
    width - ������ ����������� - NUM
    height - ������ ����������� - NUM
    background - �������� ����������� - TEX
    page - ��������, �� ������� ����� ������������ ������ - PAGE


#���� ������:
---

###TEX:
   
    ��� �������� � ����� � �����.
    ������: window.png
   
###NUM:

    ����� ������������� �����.
    ������: 42
    
###PAGE:

    ���� �� ���������: MAIN, SETTINGS, INFO, SERVERS � ����� ��������.
    ������: about
    
###STR:
   
    ����� �����, ��������� ���������.
    ������: "������"
    
###RSTR:
   
    ������ ����, ���������� �������� ';', ��������� ���������. ����� ���������� ��������� �������.
    ������: "���;������ ����;�������"
    
###COLOR:

    ���� � H�X �������. ������ �������� �� 6-�� ��������, ������� �� �����. ��� �������� ����� ���������� �������� #.
    ������: #9ff8e6
   