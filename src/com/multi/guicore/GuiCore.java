package com.multi.guicore;

import com.multi.guicore.data.CorruptedThemeException;
import com.multi.guicore.data.IGuiHooks;
import com.multi.guicore.data.ThemeParser;
import com.multi.guicore.data.WindowTheme;
import com.multi.guicore.elements.MainFrame;

import java.awt.*;
import java.net.URI;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class GuiCore {
    public static long loadMillis;
    public static long millis;
    private static IGuiHooks hooks;

    private MainFrame frame;

    public MainFrame getWindow() {
        return frame;
    }

    public static void registerHooks(IGuiHooks h) {
        if (!hooksRegistered()) {
            hooks = h;
        } else
            error(GuiCore.class, "Can't register " + hooks.getClass().getSimpleName() + ", hooks already registered!");

    }

    public static IGuiHooks getHooks() {
        return hooks;
    }

    public static boolean hooksRegistered() {
        return hooks != null;
    }

    public static void info(Class caller, String s) {
        System.out.println("[INFO] [" + caller.getSimpleName().toUpperCase() + "] " + s);
    }

    public static void warn(Class caller, String s) {
        System.out.println("[WARN] [" + caller.getSimpleName().toUpperCase() + "] " + s);
    }

    public static void error(Class caller, String s) {
        System.out.println("[ERROR] [" + caller.getSimpleName().toUpperCase() + "] " + s);
    }

    public static void severe(Class caller, String s) {
        System.out.println("[SEVERE] [" + caller.getSimpleName().toUpperCase() + "] " + s);
    }

    public static void shutdown() {
        if (GuiCore.hooksRegistered()) getHooks().terminateLauncher();
        warn(GuiCore.class, "Shutting down...");
        System.exit(0);
    }

    public static void openUrl(String what) {
        String info = "Opening " + what;
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URI(what));
                info += " ... success.";
            } catch (Exception e) {
                info += " ... failed.";
            }
        } else info += " ... failed.";
        info(GuiCore.class, info);
    }


    public void prepare(String theme) {
        loadMillis = System.currentTimeMillis();
        info(GuiCore.class, "Preparing...");
        this.frame = new MainFrame();
        ThemeParser.instance.readLayout(theme);

        try {
            WindowTheme.instance.validate();
        } catch (CorruptedThemeException ex) {
            ex.printStackTrace();
            severe(GuiCore.class, "Can't start with invalid theme, aborting.");
            shutdown();
        }
        info(GuiCore.class, "Loading GUI...");
        this.frame.init();
    }

    public void start() {
        this.frame.setVisible(true);
        if (GuiCore.hooksRegistered()) getHooks().guiDisplayed();
        info(GuiCore.class, "Successfully started, took " + (float) (System.currentTimeMillis() - GuiCore.loadMillis) / 1000F + " s.");
        if (Math.random() < 0.5F) info(GuiCore.class, "You are awesome.");
    }

}
