package com.multi.guicore.data;

/**
 * Created by MultiMote on 05.11.2014.
 */
public class ServerAddress {
    private final String ip;
    private final String port;
    private final String name;
    private final ServerInfo info;

    public ServerAddress (String ip, String port, String name){
        this.ip = ip;
        this.port = port;
        this.name = name;
        this.info = new ServerInfo();
    }

    public static ServerAddress create(String ip, String port, String name){
        return new ServerAddress(ip, port, name);
    }




    public String getIP() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    public String getName() {
        return name;
    }

    public ServerInfo getServerInfo() {
        return info;
    }

    @Override
    public String toString() {
        return "$[@"+this.name + " " + this.ip + ":" + this.port + "]";
    }

}
