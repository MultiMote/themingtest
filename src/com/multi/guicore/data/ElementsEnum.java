package com.multi.guicore.data;

/**
 * Created by MultiMote on 24.10.2014.
 */
public enum  ElementsEnum {
    MAIN_WINDOW,
    CLOSE_BUTTON,
    MINIMIZE_BUTTON(true),
    LOGIN_BUTTON,
    SERVER_LIST_BUTTON,
    SERVER_LIST,
    SERVER_INFO_PAGE,
    SETTINGS_BUTTON(true),
    HOME_BUTTON(true),
    INFO_BUTTON(true),
    USERNAME_FIELD,
    PASSWORD_FIELD,
    PASSWORD_SAVE_CHECKBOX;

    public final boolean canSkip;

    private ElementsEnum(boolean canSkip){
        this.canSkip = canSkip;
    }

    private ElementsEnum(){
        this(false);
    }

    public static ElementsEnum parseElement(String s){
        s = s.replaceAll(":", "").trim();
        for(ElementsEnum element : ElementsEnum.values()){
            if(element.toString().toLowerCase().equals(s))return element;
        }
        return null;
    }
}
