package com.multi.guicore.data;

/**
* Created by MultiMote on 04.11.2014.
*/
public enum PagesEnum {
    MAIN,
    SETTINGS,
    INFO,
    SERVER_INFO,
    SERVERS,
    LOG,
    ALL;

    public static PagesEnum parsePage(Object s){
        if(s == null) return ALL;
        s = s.toString().trim().toLowerCase();
        for(PagesEnum element : PagesEnum.values()){
            if(element.toString().toLowerCase().equals(s))return element;
        }
        return ALL;
    }
}
