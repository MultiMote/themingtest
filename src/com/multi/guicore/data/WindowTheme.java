package com.multi.guicore.data;

import java.util.HashMap;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class WindowTheme {
    public static WindowTheme instance = new WindowTheme();
    public HashMap<ElementsEnum, ThemedElement> elementMap;

    public HashMap<Integer, ThemedElement> labelMap;
    public HashMap<Integer, ThemedElement> linkMap;
    public HashMap<Integer, ThemedElement> picMap;

    public void checkMaps(){
        if(this.labelMap == null) this.labelMap = new HashMap<Integer, ThemedElement>();
        if(this.linkMap == null) this.linkMap = new HashMap<Integer, ThemedElement>();
        if(this.picMap == null) this.picMap = new HashMap<Integer, ThemedElement>();
    }

    public boolean elementsCreated(){return this.elementMap != null && !this.elementMap.isEmpty();}

    public void validate() throws CorruptedThemeException
    {
        if(!this.elementsCreated())throw new CorruptedThemeException("Theme does not contain elements!");
        for(ElementsEnum element : ElementsEnum.values()){
            if(!element.canSkip && !this.elementMap.containsKey(element))
                throw new CorruptedThemeException("Theme does not contain required element: " + element.toString().toLowerCase());
        }
    }
}
