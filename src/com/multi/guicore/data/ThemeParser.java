package com.multi.guicore.data;

import com.multi.guicore.GuiCore;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class ThemeParser {
    public static String THEME_BASE_PATH = "com/multi/guicore/theme";
    public static ThemeParser instance = new ThemeParser();

    public void readLayout(String theme) {
        long millis = System.currentTimeMillis();
        GuiCore.info(this.getClass(), "Parsing theme " + theme + "...");
        InputStream is = null;
        BufferedReader reader = null;
        try {
            is = getClass().getClassLoader().getResourceAsStream(THEME_BASE_PATH + "/" + theme + "/layout.scheme");
            if (is == null) {
                GuiCore.info(this.getClass(), "No layout.scheme for theme " + theme);
                return;
            }
            reader = new BufferedReader(new InputStreamReader(is, "UTF8"));
            String line;
            int lineNumber = 0;
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                line = line.trim();
                if (!line.isEmpty() && !line.startsWith("#") && !line.startsWith("//")) {
                    if (!this.parseLine(theme, line)) {
                        GuiCore.warn(this.getClass(), "Skipping line " + lineNumber + " of theme "+ theme +".");
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (reader != null) try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        GuiCore.info(this.getClass(), "Theme parsing finished, took " + (float) (System.currentTimeMillis() - millis) / 1000F + " s.");
    }

    public boolean parseLine(String theme, String line) {

        if (WindowTheme.instance.elementMap == null)
            WindowTheme.instance.elementMap = new HashMap<ElementsEnum, ThemedElement>();

        String[] splitted = null;
        if (line.contains(" ")) splitted = line.split(" ");
        if (splitted == null) return false;
        ElementsEnum element = ElementsEnum.parseElement(splitted[0]);
        return this.parseLineElement(theme, Arrays.asList(splitted), line, element, splitted[0].trim());
    }

    public boolean parseLineElement(String theme, List<String> tags, String raw, ElementsEnum type, String typeString) {
        if (type == ElementsEnum.MAIN_WINDOW) {
            String title = this.getTagQuotesValue("title", raw, tags);
            Image bg = this.getTagImage(theme, "background", tags, type);
            Image icon = this.getTagImage(theme, "icon", tags, type);
            int width = this.getTagInt("width", tags);
            int height = this.getTagInt("height", tags);

            if (width < 1 || height < 1) return false;

            ThemedElement element = new ThemedElement(0, 0, width, height);
            element.putProperty(PropertiesEnum.BACKGROUND, bg);
            element.putProperty(PropertiesEnum.ICON, icon);
            element.putProperty(PropertiesEnum.TEXT, title == null ? "ЕГГОГ" : title);

            WindowTheme.instance.elementMap.put(type, element);
            return true;
        } else if (type == ElementsEnum.CLOSE_BUTTON) {
            this.parseButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.MINIMIZE_BUTTON) {
            this.parseButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.LOGIN_BUTTON) {
            this.parseButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.SERVER_LIST_BUTTON) {
            this.parseServerButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.SETTINGS_BUTTON) {
            this.parseButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.HOME_BUTTON) {
            this.parseButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.INFO_BUTTON) {
            this.parseButton(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.USERNAME_FIELD) {
            this.parseTextField(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.PASSWORD_FIELD) {
            this.parseTextField(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.PASSWORD_SAVE_CHECKBOX) {
            this.parseCheckbox(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.SERVER_LIST) {
            this.parseServerList(theme, raw, tags, type);
            return true;
        } else if (type == ElementsEnum.SERVER_INFO_PAGE) {
            this.parseServerInfoPage(theme, raw, tags, type);
            return true;
        } else if(typeString.startsWith("label_")){
            this.parseLabel(theme, raw, tags, type);
            return true;
        } else if(typeString.startsWith("link_")){
            this.parseLink(theme, raw, tags, type);
            return true;
        } else if(typeString.startsWith("image_")){
            this.parseImage(theme, raw, tags, type);
            return true;
        }
        return false;
    }

    private void parseServerInfoPage(String theme, String raw, List<String> tags, ElementsEnum type) {
        int titleX = this.getTagInt("titleX", tags);
        int titleY = this.getTagInt("titleY", tags);
        int titleColor = this.getTagHEXInt("titleColor", tags);
        int titleSize = this.getTagInt("titleTextSize", tags);
        int descriptionX = this.getTagInt("descriptionX", tags);
        int descriptionY = this.getTagInt("descriptionY", tags);
        int descriptionWidth = this.getTagInt("descriptionWidth", tags);
        int descriptionHeight = this.getTagInt("descriptionHeight", tags);
        String titleFont = this.getTagQuotesValue("titleFont", raw, tags);

        int descrColor = this.getTagHEXInt("descriptionTextColor", tags);
        int descrSize = this.getTagInt("descriptionTextSize", tags);
        String descrFont = this.getTagQuotesValue("descriptionTextFont", raw, tags);

        Image buttonUpImage = this.getTagImage(theme, "upButtonBackground", tags, type);
        Image buttonDownImage = this.getTagImage(theme, "downButtonBackground", tags, type);
        Image thumbImage = this.getTagImage(theme, "thumbBackground", tags, type);

        ThemedElement element = new ThemedElement(0, 0, 0, 0);

        element.putProperty(PropertiesEnum.X_SERVER_INFO_TITLE, titleX);
        element.putProperty(PropertiesEnum.Y_SERVER_INFO_TITLE, titleY);

        element.putProperty(PropertiesEnum.X_SERVER_INFO_DESCRIPTION, descriptionX);
        element.putProperty(PropertiesEnum.Y_SERVER_INFO_DESCRIPTION, descriptionY);
        element.putProperty(PropertiesEnum.WIDTH_SERVER_INFO_DESCRIPTION, descriptionWidth);
        element.putProperty(PropertiesEnum.HEIGHT_SERVER_INFO_DESCRIPTION, descriptionHeight);

        element.putProperty(PropertiesEnum.COLOR_SERVER_INFO_TITLE, titleColor);
        element.putProperty(PropertiesEnum.FONT_SIZE_SERVER_INFO_TITLE, titleSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY_SERVER_INFO_TITLE, titleFont);

        element.putProperty(PropertiesEnum.COLOR_SERVER_INFO_DESCRIPTION, descrColor);
        element.putProperty(PropertiesEnum.FONT_SIZE_SERVER_INFO_DESCRIPTION, descrSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY_SERVER_INFO_DESCRIPTION, descrFont);

        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_INFO_UP_BUTTON, buttonUpImage);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_INFO_DOWN_BUTTON, buttonDownImage);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_INFO_THUMB, thumbImage);

        WindowTheme.instance.elementMap.put(type,
                element);
    }


    private void parseServerList(String theme, String raw, List<String> tags, ElementsEnum type) {
        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int width = this.getTagInt("width", tags);
        int height = this.getTagInt("height", tags);

        int xButton = this.getTagInt("infoButtonX", tags);
        int yButton = this.getTagInt("infoButtonY", tags);
        int widthButton = this.getTagInt("infoButtonWidth", tags);
        int heightButton = this.getTagInt("infoButtonHeight", tags);

        int widthNode = this.getTagInt("nodeWidth", tags);
        int heightNode = this.getTagInt("nodeHeight", tags);

        int marginLeft = this.getTagInt("nodeMarginLeft", tags);
        int marginTop = this.getTagInt("nodeMarginTop", tags);
        int spacing = this.getTagInt("nodeSpacing", tags);
        int nodeTextColor = this.getTagHEXInt("nodeTextColor", tags);


        Image image = null;

        if(!this.hasTag("nobg", tags))image = this.getTagImage(theme, "background", tags, type);

        Image buttonImage = this.getTagImage(theme, "infoButtonBackground", tags, type);
        Image nodeImage = this.getTagImage(theme, "nodeBackground", tags, type);
        Image buttonUpImage = this.getTagImage(theme, "upButtonBackground", tags, type);
        Image buttonDownImage = this.getTagImage(theme, "downButtonBackground", tags, type);
        Image thumbImage = this.getTagImage(theme, "thumbBackground", tags, type);


        ThemedElement element = new ThemedElement(x, y, width, height);

        element.putProperty(PropertiesEnum.BACKGROUND, image);

        element.putProperty(PropertiesEnum.X_SERVER_LIST_INFO_BUTTON, xButton);
        element.putProperty(PropertiesEnum.Y_SERVER_LIST_INFO_BUTTON, yButton);
        element.putProperty(PropertiesEnum.WIDTH_SERVER_LIST_INFO_BUTTON, widthButton);
        element.putProperty(PropertiesEnum.HEIGHT_SERVER_LIST_INFO_BUTTON, heightButton);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_LIST_INFO_BUTTON, buttonImage);

        element.putProperty(PropertiesEnum.MARGIN_LEFT_SERVER_LIST_NODE, marginLeft);
        element.putProperty(PropertiesEnum.MARGIN_TOP_SERVER_LIST_NODE, marginTop);
        element.putProperty(PropertiesEnum.SPACING_NODE, spacing);
        element.putProperty(PropertiesEnum.WIDTH_SERVER_LIST_NODE, widthNode);
        element.putProperty(PropertiesEnum.HEIGHT_SERVER_LIST_NODE, heightNode);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_LIST_NODE, nodeImage);
        element.putProperty(PropertiesEnum.FONT_COLOR, nodeTextColor);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_LIST_UP_BUTTON, buttonUpImage);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_LIST_DOWN_BUTTON, buttonDownImage);
        element.putProperty(PropertiesEnum.BACKGROUND_SERVER_LIST_THUMB, thumbImage);

        WindowTheme.instance.elementMap.put(type,
                element);
    }



    private void parseCheckbox(String theme, String raw, List<String> tags, ElementsEnum type) {
        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int textColor = this.getTagHEXInt("textColor", tags);
        int textSize = this.getTagInt("textSize", tags);
        String text = this.getTagQuotesValue("text", raw, tags);
        String font = this.getTagQuotesValue("font", raw, tags);
        Image[] images = new Image[]{
                this.getTagImage(theme, "background", tags, type),
                this.getTagImage(theme, "selected", tags, type)
        };

        ThemedElement element = new ThemedElement(x, y, 0, 0);
        element.putProperty(PropertiesEnum.TEXT, text);
        element.putProperty(PropertiesEnum.FONT_COLOR, textColor);
        element.putProperty(PropertiesEnum.FONT_SIZE, textSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY, font);
        element.putProperty(PropertiesEnum.BACKGROUND_CHECKBOX_NORMAL, images[0]);
        element.putProperty(PropertiesEnum.BACKGROUND_CHECKBOX_SELECTED, images[1]);

        WindowTheme.instance.elementMap.put(type, element);
    }

    private void parseImage(String theme, String raw, List<String> tags, ElementsEnum type) {
        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int width = this.getTagInt("width", tags);
        int height = this.getTagInt("height", tags);

        String page = this.getTagValue("page", tags);
        Image image = this.getTagImage(theme, "background", tags, type);

        ThemedElement element = new ThemedElement(x, y, width, height);

        element.putProperty(PropertiesEnum.BACKGROUND, image);
        element.putProperty(PropertiesEnum.PAGE, page);

        WindowTheme.instance.checkMaps();
        WindowTheme.instance.picMap.put(WindowTheme.instance.picMap.size(),
                element);
    }

    private void parseLabel(String theme, String raw, List<String> tags, ElementsEnum type) {
        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int textColor = this.getTagHEXInt("textColor", tags);
        int textSize = this.getTagInt("textSize", tags);
        String text = this.getTagQuotesValue("text", raw, tags);
        String font = this.getTagQuotesValue("font", raw, tags);
        String page = this.getTagValue("page", tags);

        ThemedElement element = new ThemedElement(x, y, 0, 0);
        element.putProperty(PropertiesEnum.TEXT, text);
        element.putProperty(PropertiesEnum.FONT_COLOR, textColor);
        element.putProperty(PropertiesEnum.FONT_SIZE, textSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY, font);
        element.putProperty(PropertiesEnum.PAGE, page);


        WindowTheme.instance.checkMaps();
        WindowTheme.instance.labelMap.put(WindowTheme.instance.labelMap.size(), element);
    }

    private void parseLink(String theme, String raw, List<String> tags, ElementsEnum type) {
        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int textColor = this.getTagHEXInt("textColor", tags);
        int textSize = this.getTagInt("textSize", tags);
        String text = this.getTagQuotesValue("text", raw, tags);
        String link = this.getTagQuotesValue("link", raw, tags);
        String font = this.getTagQuotesValue("font", raw, tags);
        String page = this.getTagValue("page", tags);

        ThemedElement element = new ThemedElement(x, y, 0, 0);
        element.putProperty(PropertiesEnum.TEXT, text);
        element.putProperty(PropertiesEnum.FONT_COLOR, textColor);
        element.putProperty(PropertiesEnum.FONT_SIZE, textSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY, font);
        element.putProperty(PropertiesEnum.LINK, link);
        element.putProperty(PropertiesEnum.PAGE, page);

        WindowTheme.instance.checkMaps();
        WindowTheme.instance.linkMap.put(WindowTheme.instance.linkMap.size(), element);
    }

    private void parseTextField(String theme, String raw, List<String> tags, ElementsEnum type) {


        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int width= this.getTagInt("width", tags);
        int height = this.getTagInt("height", tags);
        int textMarginLeft = this.getTagInt("textMarginLeft", tags);
        int textMarginRight = this.getTagInt("textMarginRight", tags);
        int textMarginTop = this.getTagInt("textMarginTop", tags);
        int textMarginBottom = this.getTagInt("textMarginBottom", tags);
        int textColor = this.getTagHEXInt("textColor", tags);
        int textSize = this.getTagInt("textSize", tags);
        int pointerColor = this.getTagHEXInt("pointerColor", tags);
        String font = this.getTagQuotesValue("font", raw, tags);

        Image image = this.getTagImage(theme, "background", tags, type);

        ThemedElement element = new ThemedElement(x, y, width, height);
        element.putProperty(PropertiesEnum.BACKGROUND, image);
        element.putProperty(PropertiesEnum.FONT_COLOR, textColor);
        element.putProperty(PropertiesEnum.FONT_SIZE, textSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY, font);
        element.putProperty(PropertiesEnum.INSET_LEFT, textMarginLeft);
        element.putProperty(PropertiesEnum.INSET_RIGHT, textMarginRight);
        element.putProperty(PropertiesEnum.INSET_TOP, textMarginTop);
        element.putProperty(PropertiesEnum.INSET_BOTTOM, textMarginBottom);
        element.putProperty(PropertiesEnum.CARET_COLOR, pointerColor);

        WindowTheme.instance.elementMap.put(type, element);
    }

    private void parseServerButton(String theme, String raw, List<String> tags, ElementsEnum type) {
        Image[] images= new Image[4];

        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int width = this.getTagInt("width", tags);
        int height = this.getTagInt("height", tags);
        String staticBg = this.getTagValue("static", tags);
        String text = this.getTagQuotesValue("text", raw, tags);
        int textColor = this.getTagHEXInt("textColor", tags);
        int textSize = this.getTagInt("textSize", tags);
        String font = this.getTagQuotesValue("font", raw, tags);

        if (staticBg != null)
            images[3] = this.getTagImage(theme, "static", tags, type);
        else {
            images[0] = this.getTagImage(theme, "background", tags, type);
            images[1] = this.getTagImage(theme, "hover", tags, type);
            images[2] = this.getTagImage(theme, "pressed", tags, type);
        }

        ThemedElement element = new ThemedElement(x, y, width, height);
        element.putProperty(PropertiesEnum.BACKGROUND_STATIC, images[3]);
        element.putProperty(PropertiesEnum.BACKGROUND, images[0]);
        element.putProperty(PropertiesEnum.BACKGROUND_HOVER, images[1]);
        element.putProperty(PropertiesEnum.BACKGROUND_PRESSED, images[2]);
        element.putProperty(PropertiesEnum.FONT_COLOR, textColor);
        element.putProperty(PropertiesEnum.FONT_SIZE, textSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY, font);

        WindowTheme.instance.elementMap.put(type, element);
    }

    private void parseButton(String theme, String raw, List<String> tags, ElementsEnum type) {
        Image[] images= new Image[4];

        int x = this.getTagInt("x", tags);
        int y = this.getTagInt("y", tags);
        int width = this.getTagInt("width", tags);
        int height = this.getTagInt("height", tags);
        String staticBg = this.getTagValue("static", tags);
        String text = this.getTagQuotesValue("text", raw, tags);
        int textColor = this.getTagHEXInt("textColor", tags);
        int textSize = this.getTagInt("textSize", tags);
        String font = this.getTagQuotesValue("font", raw, tags);

        if(!this.hasTag("nobg", tags)){

        if (staticBg != null)
            images[3] = this.getTagImage(theme, "static", tags, type);
        else {
            images[0] = this.getTagImage(theme, "background", tags, type);
            images[1] = this.getTagImage(theme, "hover", tags, type);
            images[2] = this.getTagImage(theme, "pressed", tags, type);
        }

        }

        ThemedElement element = new ThemedElement(x, y, width, height);
        element.putProperty(PropertiesEnum.TEXT, text);
        element.putProperty(PropertiesEnum.BACKGROUND_STATIC, images[3]);
        element.putProperty(PropertiesEnum.BACKGROUND, images[0]);
        element.putProperty(PropertiesEnum.BACKGROUND_HOVER, images[1]);
        element.putProperty(PropertiesEnum.BACKGROUND_PRESSED, images[2]);
        element.putProperty(PropertiesEnum.FONT_COLOR, textColor);
        element.putProperty(PropertiesEnum.FONT_SIZE, textSize);
        element.putProperty(PropertiesEnum.FONT_FAMILY, font);

        WindowTheme.instance.elementMap.put(type, element);
    }

    private int parsePositiveInt(String s) {
        int i = 0;
        if(s==null) return i;
        try {
            i = Integer.parseInt(s);
        } catch (NumberFormatException ignored) {
        }
        return i < 0 ? 0 : i;
    }

    private int parsePositiveHEXInt(String s) {
        int i = 0;
        if(s==null) return i;
        try {
            i = Integer.parseInt(s.replaceAll("#", ""), 16);
        } catch (NumberFormatException ignored) {
        }
        return i < 0 ? 0 : i;
    }

    private String getTagValue(String tag, List<String> tagList) {
        if (tagList.contains(tag)) {
            int tagIndex = tagList.indexOf(tag);
            if (this.isIndexInList(tagIndex + 1, tagList)) return tagList.get(tagIndex + 1);
        }
        return null;
    }

    private boolean hasTag(String tag, List<String> tagList){
        return tagList.contains(tag);
    }

    private String getTagQuotesValue(String tag, String raw, List<String> tagList) {
        tag = " " + tag + " ";
        if (raw.contains(tag)) {
            int pos = raw.lastIndexOf(tag) + tag.length();
            if(raw.charAt(pos) != '\"')return null;
            String val = "";
            pos++;
            boolean foundEnd = false;
            do{
                char next = raw.charAt(pos++);
                if(next == '\"'){
                    foundEnd = true;
                    break;
                }
                else
                val+= next;

            }while (pos < raw.length());

            if(foundEnd)return val; else return null;
        }
        return null;
    }

    private int getTagInt(String tag, List<String> tagList){
        String val = this.getTagValue(tag, tagList);
        if(tag!=null) return this.parsePositiveInt(val);
        return 0;
    }

    private int getTagHEXInt(String tag, List<String> tagList){
        String val = this.getTagValue(tag, tagList);
        if(tag!=null) return this.parsePositiveHEXInt(val);
        return 0;
    }

    private boolean isIndexInList(int index, List list) {
        return index > 0 && index < list.size();
    }

    private Image getTagImage(String theme, String tag, List<String> tags, ElementsEnum type) {
        return this.getTagImage(theme, tag, tags, type, false);
    }

    private Image getTagImage(String theme, String tag, List<String> tags, ElementsEnum type, boolean dontWarnIfNotFound) {
        String val = this.getTagValue(tag, tags);
        if(val == null){
            if(!dontWarnIfNotFound)GuiCore.error(this.getClass(), "Can't read image " + tag + ", tag not found, element " + type.toString().toLowerCase() + " of theme " + theme + "!");
            return null;
        }
        return this.readImage(theme, tag, val, type);
    }

    private Image readImage(String theme, String tag, String value,  ElementsEnum type) {
        InputStream is = null;
        try {
            is = getClass().getClassLoader().getResourceAsStream(THEME_BASE_PATH + "/" + theme + "/" + value);
            if (is == null) throw new IOException();
            return ImageIO.read(is);
        } catch (IOException e) {
            GuiCore.error(this.getClass(), "Can't read image " + tag + " (" + value + ") for element " + type.toString().toLowerCase() + " of theme " + theme + "!");
        } finally {
            if (is != null) try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
