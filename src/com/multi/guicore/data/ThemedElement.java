package com.multi.guicore.data;

import java.awt.*;
import java.util.HashMap;

/**
 * Created by MultiMote on 24.10.2014.
 */
public class ThemedElement {
    public final int xPos;
    public final int yPos;
    public final int width;
    public final int height;
    private HashMap<PropertiesEnum, Object> properties;


    public ThemedElement(int xPos, int yPos, int width, int height){
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.properties = new HashMap<PropertiesEnum, Object>();
    }

    public Rectangle getBounds(){
        return new Rectangle(this.xPos, this.yPos, this.width, this.height);
    }

    public void putProperty(PropertiesEnum property, Object value) {
        this.properties.put(property, value);
    }

    public Object getProperty(PropertiesEnum property){
        if(this.properties.containsKey(property))return this.properties.get(property);
        return null;
    }

    public String getPropertyString(PropertiesEnum property){
        if(this.properties.containsKey(property))
            return (String)this.properties.get(property);
        return null;
    }

    public int getPropertyInt(PropertiesEnum property){
        if(this.properties.containsKey(property))
            return (Integer)this.properties.get(property);
        return 0;
    }

    public Color getPropertyColor(PropertiesEnum property){
       return new Color(this.getPropertyInt(property));
    }

    public Image getPropertyImage(PropertiesEnum property){
        if(this.properties.containsKey(property))
            return (Image)this.properties.get(property);
        return null;
    }


    @Override
    public String toString() {
       return getClass().getName() + "[x=" + this.xPos + ",y=" + this.yPos + ",width=" + this.width + ",height=" + this.height + "]";
    }
}
