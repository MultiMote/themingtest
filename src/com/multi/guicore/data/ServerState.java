package com.multi.guicore.data;

/**
* Created by MultiMote on 08.11.2014.
*/
public enum ServerState {
    ACTIVE,
    MAINTENANCE("maintenance"),
    STOPPED("stopped"),
    TIMED_OUT("timeout"),
    POLLING("polling...");
    public final String label;

    private ServerState(String label){
        this.label = label;
    }

    private ServerState(){
        this.label = "";
    }

    public String translateState(ElementsEnum element, ThemedElement theme){
        if(element == ElementsEnum.SERVER_LIST){

        }else if(element == ElementsEnum.SERVER_LIST){

        }
        return this.label;
    }
}
