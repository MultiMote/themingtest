package com.multi.guicore.data;

/**
 * Created by MultiMote on 03.11.2014.
 */
public interface IGuiHooks {

    /**
     * Kill your threads here.
     */
    public void terminateLauncher();

    public void guiDisplayed();

    /**
     * Return true to cancel event.
     */
    public boolean closeClick();

    /**
     * Return true to cancel event.
     */
    public boolean minimizeClick();

    /**
     * Return true to cancel event.
     */
    public void passwordCheckboxClick(boolean selected);

    /**
     * Return true to cancel event.
     */
    public boolean linkClick(String link);

    /**
     * Return true to cancel event.
     */
    public boolean changePage(PagesEnum from, PagesEnum to);

    public void loginButtonClick(String text, String password, ServerAddress server);

    public boolean serverClick(ServerAddress address);
    public boolean serverInfoClick(ServerAddress address);
    public boolean serverListButtonClick();
}
