package com.multi.guicore.data;

/**
 * Created by MultiMote on 25.10.2014.
 */
public class CorruptedThemeException extends RuntimeException {
    public CorruptedThemeException(String s){
        super(s);
    }
}
