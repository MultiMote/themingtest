package com.multi.guicore.data;

import javax.swing.text.AttributeSet;
import javax.swing.text.PlainDocument;
import java.util.regex.Pattern;

/**
 * Created by MultiMote on 03.11.2014.
 */
public class TextFilter extends PlainDocument {
    private Pattern p;

    public TextFilter(String regexp){
        this.p = Pattern.compile(regexp);
    }

    @Override
    public void insertString(int pos, String insert, AttributeSet as) {
        try {
            if (!p.matcher(getText(0, getLength()) + insert).find()) {
                super.insertString(pos, insert, as);
            }
        } catch (Exception ignored) {}

    }
}