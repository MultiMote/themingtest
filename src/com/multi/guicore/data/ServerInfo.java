package com.multi.guicore.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by MultiMote on 05.11.2014.
 */
public class ServerInfo {
    private String description;
    private int playersMax;
    private int ping = -1;
    private List<String> players = new ArrayList<String>();
    private ServerState state;

    public ServerInfo(){
        this.description = "";
        this.state = ServerState.POLLING;
    }

    public ServerInfo(String description, int playersMax){
        this.description = description;
        this.playersMax = playersMax;
        this.state = ServerState.POLLING;
    }

    public void setState(ServerState state) {
        this.state = state;
    }

    public String translateState(ElementsEnum element, ThemedElement theme){
       String str = "err";
        if(this.getState() == ServerState.ACTIVE){
            str = "Пинг: " + this.getPing() + " ms.";
        } else str = this.getState().translateState(element, theme);

        return str;

    }

    public ServerState getState() {
        return state;
    }

    public String getDescription() {
        return description;
    }

    public int getPlayersMax() {
        return playersMax;
    }

    public List<String> getPlayers() {
        return players;
    }

    public int getPing() {
        return ping;
    }

    public ServerInfo updatePing(int ping) {
        this.ping = ping;
        return this;
    }

    public ServerInfo setPlayersMax(int playersMax) {
        this.playersMax = playersMax;
        return this;
    }

    public ServerInfo setDescription(String description) {
        this.description = description;
        return this;

    }

    public ServerInfo setPlayers(List<String> players) {
        this.players = players;
        return this;
    }

    public ServerInfo clearPlayers() {
        this.players.clear();
        return this;
    }

    public ServerInfo addPlayers(String ... players) {
        this.players.addAll(Arrays.asList(players));
        return this;
    }


}
