package com.multi.guicore;

import com.multi.guicore.data.ServerAddress;
import com.multi.guicore.data.ServerState;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by MultiMote on 03.11.2014.
 */
public class Launch {
    static boolean state;
    public static String ENDL = "\n\r";

    public static void main(String[] args) {
        GuiCore.registerHooks(new ExampleHooks());

        final GuiCore core = new GuiCore();

       // core.prepare("main");
        core.prepare("ims");

        core.getWindow().setPassSaving(true);
        core.getWindow().setUsername("RedDragon");
        core.getWindow().setPassword("mypass");
        ArrayList<ServerAddress> servers = new ArrayList<ServerAddress>();

        ServerAddress[] addresses = new ServerAddress[]{
                ServerAddress.create("localhost", "25566", "Best"),
                ServerAddress.create("127.0.0.1", "25566", "Worst"),
                ServerAddress.create("127.0.0.1", "25566", "Something"),
                ServerAddress.create("127.0.0.1", "25566", "Servers"),
                ServerAddress.create("127.0.0.1", "25566", "For"),
                ServerAddress.create("127.0.0.1", "25566", "Scrollbar"),
                ServerAddress.create("127.0.0.1", "25566", "Check")
        };

        addresses[0].getServerInfo()
                .setDescription("Вообще самый лучший сервер ever." + ENDL + ENDL +
                "Моды:" + ENDL +
                "- никаких" + ENDL +
                "- никаких" + ENDL +
                "- никаких" + ENDL +
                "- никаких" + ENDL +
                "- никаких" + ENDL)
                .addPlayers("Vasyan", "Vano", "Vovan")
                .updatePing(42)
                .setPlayersMax(10);


        addresses[1].getServerInfo()
                .setDescription("worst server")
                .addPlayers("Killer", "Santa", "Planshit", "IPphone")
                .updatePing(63)
                .setPlayersMax(10);

        servers.addAll(Arrays.asList(addresses));


        core.getWindow().addServerList(servers);

        core.getWindow().setCurrentServer(addresses[0]);

        core.start();


        Timer timer = new Timer(3000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                state = !state;

                    for (ServerAddress a : core.getWindow().getServers()) {

                        if (Math.random() < 0.6F && state) {
                        a.getServerInfo().updatePing((int) (Math.random() * 500F));
                        a.getServerInfo().setState(ServerState.ACTIVE);
                        }else
                        a.getServerInfo().setState(ServerState.POLLING);
                    }

                core.getWindow().updateAllDisplays();
            }
        });
        timer.start();
    }
}
